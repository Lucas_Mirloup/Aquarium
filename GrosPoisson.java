public class GrosPoisson extends Vivant{
    GrosPoisson(double x, double y,double v){
        this.posX = x;
        this.posY = y;
        this.vitesseX = v;
    }
    @Override
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        String c;
        if(vitesseX>=0){
            c="       \\\\^^^^^\\\\\n" +
                    "     ,oO((((((((Oo,\n" +
                    "}\\\\ ,(((((((((((((((',\n" +
                    "}=((((((((((((((((  o',\n" +
                    "}=((((((((((((((((    <\n" +
                    "}=(((((((/ /((((((   ,'\n" +
                    "}/ '(((((\\\\/((((((((,'\n" +
                    "    '*O((((((((((O*'";
}
        else {
            c = "        /^^^^^7\n" +
                    "    ,oO))))))))Oo,\n" +
                    "  ,'))))))))))))))), /{\n" +
                    ",'o  ))))))))))))))))={\n" +
                    ">    ))))))))))))))))={\n" +
                    "`,   ))))))\\ \\)))))))={\n" +
                    "  ',))))))))\\/)))))' \\{\n" +
                    "    '*O))))))))O*'";
        }
        e.ajouteChaine((int)posX,(int)posY,c,"salmon");
        return e;
    }
    public double getPosX() {
        if(vitesseX>=0){return posX+20;}
        else{return posX;}
    }

    @Override
    public Bulle genereBulle(){
        if(vitesseX>=0){
            return new Bulle(posX+20,posY-9);
        }
        else{
            return new Bulle(posX-1,posY-9);
        }
    }
}