import java.util.ArrayList;

public class EnsembleChaines {
    ArrayList<ChainePositionnee> chaines;
    public EnsembleChaines(){chaines= new ArrayList<ChainePositionnee>(); }

    public void ajouteChaine(int x, int y, String c, String couleur){
        chaines.add(new ChainePositionnee(x,y,c,couleur));}

    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }


    public boolean contient(int posx, int posy){
        for(ChainePositionnee c: chaines){
            if (c.posX<=posx  && c.posY == posy && posx < c.posX+c.chaine.length())
                return true;
        }
        return false;

    }
    public void vider(){ chaines.clear();}
}