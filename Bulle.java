public class Bulle extends Vivant {
    Bulle(double x, double y){
        posX=x;
        posY=y;
    }
    public void evolue(int hauteur) {
        posY += 5;
    }
    @Override
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        e.ajouteChaine((int)posX,(int)posY,"°","#000");
        return e;
    }
}
