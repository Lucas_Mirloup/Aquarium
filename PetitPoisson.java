public class PetitPoisson extends Vivant {
    PetitPoisson(double x, double y, double v){
        posX = x;
        posY = y;
        vitesseX = v;
    }

    @Override
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        String c;
        if(vitesseX>=0){c="><)))'>";}
        else{c="<'(((><";}
        e.ajouteChaine((int)posX,(int)posY,c,"red");

        /*if(Math.random()<0.01){
            ChainePositionnee parole = parler();
            e.ajouteChaine(parole.posX,parole.posY,parole.chaine,parole.couleur);
        }*/

        return e;
    }

    @Override
    public double getPosX() {
        if(vitesseX>=0){return posX+7;}
        else{return posX;}
    }

    @Override
    public Bulle genereBulle(){
        if(vitesseX>=0){
            return new Bulle(posX+6,posY-5);
        }
        else{
            return new Bulle(posX-1,posY-5);
        }
    }

    /*public ChainePositionnee parler(){
        String mot=
                "/----------\\\n" +
                "|   Bloub   |\n" +
                "\\----------/";
        if(vitesseX>=0){
            return new ChainePositionnee((int)posX+6,(int)posY-5,mot,"black");
        }
        else{
            return new ChainePositionnee((int)posX-1,(int)posY-5,mot,"black");
        }
    }*/
}