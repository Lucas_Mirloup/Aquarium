import java.util.ArrayList;

public class Aquarium {
    private ArrayList<ObjAqua> ensObjs;
    private ArrayList<Vivant> ensVivant;
    private ArrayList<Bulle> ensBulles;
    public Aquarium(){
        this.ensObjs = new ArrayList<ObjAqua>();
        this.ensVivant = new ArrayList<Vivant>();
        this.ensBulles = new ArrayList<Bulle>();

        ensObjs.add(new Chateau(0));
        ensObjs.add(new Chateau(getLargeur()-33));

        for(int i=0;i<getLargeur();i+=2) {
            if (Math.random() < 0.25 && ensObjs.size()<30) {
                ensObjs.add(new Algue(i));
            }
        }
        for(int i=0;i<12;i++){
            ensVivant.add(new PetitPoisson((Math.random()*(getLargeur()-20))+10,(Math.random()*(getHauteur()-10))+5,(Math.random()*5)-2.5));
        }
        for(int i=0;i<3;i++){
            ensVivant.add(new GrosPoisson((Math.random()*(getLargeur()-40))+20,(Math.random()*(getHauteur()-20))+10,(Math.random()*5)-2.5));
        }
    }
    public int getHauteur(){return 50;}
    public int getLargeur(){return 120;}
    public EnsembleChaines getChaines(){
        EnsembleChaines res = new EnsembleChaines();
        for(ObjAqua a:ensObjs) {
            res.union(a.getEnsembleChaines());
        }
        for(Vivant a:ensVivant) {
            res.union(a.getEnsembleChaines());
        }
        for(Bulle a:ensBulles){
            res.union(a.getEnsembleChaines());
        }
        return res;
    }
    public void evolue(){
        ArrayList<Bulle> temp = new ArrayList<Bulle>();
        for(ObjAqua a:ensObjs){
            a.evolue();
        }
        for(Vivant a:ensVivant){
            a.evolue();
            if(a.getPosX()<=0 || a.getPosX()>getLargeur()){
                a.goBack();
            }
            if(Math.random() < 0.05) {
                ensBulles.add(a.genereBulle());
            }
        }
        for(Bulle a:ensBulles){
            a.evolue(getHauteur());
            if(a.getPosY()>=getHauteur()){
                temp.add(a);
            }
        }
        for(Bulle a:temp){
            ensBulles.remove(a);
        }
    }
}