public class Algue extends ObjAqua{
    private int frame;
    private int random;
    private String chaine1;
    private String chaine2;

    public Algue(double x){
        random = (((int)(Math.random()*5)*2)+2);
        this.posX = x;
        this.posY = random+2;
        this.frame = 0;
        chaine1="(\n )\n";
        chaine2=" )\n(\n";
        for (int i=0;i<random/2;i++){
            chaine1+="(\n )\n";
            chaine2+=" )\n(\n";
        }
    }
    @Override
    public void evolue(){
        this.frame=(this.frame+1)%2;
    }
    @Override
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        if(this.frame==0){
            e.ajouteChaine((int)posX,(int)posY,chaine1,"green");
        }
        else{
            e.ajouteChaine((int)posX,(int)posY,chaine2,"green");
        }
        return e;
    }
}
