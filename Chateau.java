public class Chateau extends ObjAqua{
    Chateau(double x){
        this.posX = x;
        this.posY = 9;
    }
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines e = new EnsembleChaines();
        e.ajouteChaine((int)posX,(int)posY,"   |Zz.                      |Zz.\n" +
                "   |                         |\n" +
                "  _L__                      _|\n" +
                " (____)      _             /__\\\n" +
                " /____\\     /_\\           /____\\\n" +
                "(______)    | |          (______)\n" +
                " |  U |_____| |___________| U  |\n" +
                " |u   | |  |   __   |  |  |   u|\n" +
                " |    | |  |  ||||  |  |  |    |\n" +
                " -------------\"\"\"\"--------------","#000");
        return e;
    }
}
