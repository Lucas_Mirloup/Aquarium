public class ObjAqua {
    protected double posX;
    protected double posY;
    protected double vitesseX;

    public void evolue(){this.posX+=vitesseX;}

    public EnsembleChaines getEnsembleChaines(){return new EnsembleChaines();}

    public double getPosX() {return posX;}
    public double getPosY() {return posY;}
}